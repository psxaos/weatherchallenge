package pl.piotrsobczynski.weatherchallenge.di.component

import dagger.Subcomponent
import pl.piotrsobczynski.weatherchallenge.di.Annotations
import pl.piotrsobczynski.weatherchallenge.di.module.WeatherFragmentModule
import pl.piotrsobczynski.weatherchallenge.usecase.displayweather.WeatherFragment

@Annotations.WeatherScope
@Subcomponent(modules = arrayOf(WeatherFragmentModule::class))
interface WeatherFragmentSubcomponent {

    fun inject(view: WeatherFragment)
}