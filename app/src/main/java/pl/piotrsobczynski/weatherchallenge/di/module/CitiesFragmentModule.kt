package pl.piotrsobczynski.weatherchallenge.di.module

import dagger.Module
import dagger.Provides
import pl.piotrsobczynski.weatherchallenge.di.Annotations
import pl.piotrsobczynski.weatherchallenge.repository.Repository
import pl.piotrsobczynski.weatherchallenge.usecase.displaycities.CitiesContract
import pl.piotrsobczynski.weatherchallenge.usecase.displaycities.CitiesPresenter
import pl.piotrsobczynski.weatherchallenge.utils.SchedulerProvider


@Module
class CitiesFragmentModule {

    @Provides
    @Annotations.CitiesScope
    fun provideCitiesPresenter(repository: Repository,
                               schedulerProvider: SchedulerProvider)
            : CitiesContract.Presenter {
        return CitiesPresenter(repository,
                schedulerProvider)
    }
}