package pl.piotrsobczynski.weatherchallenge.di.module

import dagger.Module
import dagger.Provides
import pl.piotrsobczynski.weatherchallenge.di.Annotations
import pl.piotrsobczynski.weatherchallenge.usecase.displayweather.WeatherContract
import pl.piotrsobczynski.weatherchallenge.usecase.displayweather.WeatherPresenter
import pl.piotrsobczynski.weatherchallenge.utils.SchedulerProvider

@Module
class WeatherFragmentModule {

    @Provides
    @Annotations.WeatherScope
    fun provideWeatherPresenter(schedulerProvider: SchedulerProvider): WeatherContract.Presenter {
        return WeatherPresenter(schedulerProvider)
    }
}