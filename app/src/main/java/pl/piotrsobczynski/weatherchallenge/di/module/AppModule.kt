package pl.piotrsobczynski.weatherchallenge.di.module

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.piotrsobczynski.weatherchallenge.App
import pl.piotrsobczynski.weatherchallenge.repository.Repository
import pl.piotrsobczynski.weatherchallenge.repository.RepositoryImpl
import pl.piotrsobczynski.weatherchallenge.repository.WeatherResponseMapper
import pl.piotrsobczynski.weatherchallenge.repository.remote.Remote
import pl.piotrsobczynski.weatherchallenge.repository.remote.RemoteImpl
import pl.piotrsobczynski.weatherchallenge.repository.remote.WeatherApi
import pl.piotrsobczynski.weatherchallenge.utils.SchedulerProvider
import javax.inject.Named
import javax.inject.Singleton

const val SCHEDULER_MAIN_THREAD = "mainThread"
const val SCHEDULER_IO = "io"

@Module
class AppModule(val app: App) {

    @Provides
    @Singleton
    fun provideApp(): App = app

    @Provides
    @Named(SCHEDULER_MAIN_THREAD)
    fun provideAndroidMainThreadScheduler(): Scheduler = AndroidSchedulers.mainThread()

    @Provides
    @Named(SCHEDULER_IO)
    fun provideIoScheduler(): Scheduler = Schedulers.io()

    @Provides
    fun provideScheduler(): SchedulerProvider = SchedulerProvider()

    @Provides
    fun provideRepository(remote: Remote,
                          schedulerProvider: SchedulerProvider,
                          responseMapper: WeatherResponseMapper)
            : Repository {
        return RepositoryImpl(remote, schedulerProvider, responseMapper)
    }

    @Provides
    fun provideRemote(weatherApi: WeatherApi): Remote = RemoteImpl(weatherApi)

}