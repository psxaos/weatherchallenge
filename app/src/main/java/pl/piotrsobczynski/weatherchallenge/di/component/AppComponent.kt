package pl.piotrsobczynski.weatherchallenge.di.component

import dagger.Component
import pl.piotrsobczynski.weatherchallenge.App
import pl.piotrsobczynski.weatherchallenge.di.module.AppModule
import pl.piotrsobczynski.weatherchallenge.di.module.CitiesFragmentModule
import pl.piotrsobczynski.weatherchallenge.di.module.NetModule
import pl.piotrsobczynski.weatherchallenge.di.module.WeatherFragmentModule
import pl.piotrsobczynski.weatherchallenge.utils.SchedulerProvider
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
        AppModule::class,
        NetModule::class))

interface AppComponent {
    fun inject(app: App)
    //fun inject(view: CitiesFragment)
    //fun inject(view:WeatherFragment)


    fun plus(weatherFragmentModule: WeatherFragmentModule): WeatherFragmentSubcomponent

    fun plus(citiesFragmentModule: CitiesFragmentModule): CitiesFragmentSubcomponent

    //definition what should be accessible downstream to Subcomponents

    fun schedulerProvider(): SchedulerProvider
}