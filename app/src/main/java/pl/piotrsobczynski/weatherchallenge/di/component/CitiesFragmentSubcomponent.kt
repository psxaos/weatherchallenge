package pl.piotrsobczynski.weatherchallenge.di.component

import dagger.Subcomponent
import pl.piotrsobczynski.weatherchallenge.usecase.displaycities.CitiesFragment
import pl.piotrsobczynski.weatherchallenge.di.Annotations
import pl.piotrsobczynski.weatherchallenge.di.module.CitiesFragmentModule

@Annotations.CitiesScope
@Subcomponent(modules = arrayOf(CitiesFragmentModule::class))
interface CitiesFragmentSubcomponent {

    fun inject(view: CitiesFragment)
}