package pl.piotrsobczynski.weatherchallenge.di

import javax.inject.Scope


class Annotations {

    @Scope
    @Retention(AnnotationRetention.RUNTIME)
    annotation class CitiesScope

    @Scope
    @Retention(AnnotationRetention.RUNTIME)
    annotation class WeatherScope

}