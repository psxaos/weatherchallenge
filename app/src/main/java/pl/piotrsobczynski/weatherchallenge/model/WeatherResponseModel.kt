package pl.piotrsobczynski.weatherchallenge.model

import com.google.gson.annotations.SerializedName

data class WeatherResponseModel(@SerializedName("list") val forecasts: List<Forecast>)

data class Clouds(val all: Int)

data class Main(val temp: Double,
                val pressure: Double,
                val humidity: Int,
                val temp_min: Double,
                val temp_max: Double)

data class Sys(val country: String)

class Weather(val id: Int,
              @SerializedName("main") val mainDescription: String,
              val description: String,
              val icon: String)

data class Forecast(@SerializedName("id") val cityId: Int,
                    @SerializedName("name") val cityName: String,
                    @SerializedName("country") val country: String,
                    val main: Main,
                    @SerializedName("dt") val date: Int,
                    val sys: Sys,
                    val clouds: Clouds,
                    val weather: kotlin.collections.List<Weather>)

