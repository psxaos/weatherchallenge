package pl.piotrsobczynski.weatherchallenge.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WeatherModel(val temperature: Double,
                        val tempMin: Double,
                        val tempMax: Double,
                        val humidity: Int,
                        val pressure: Double,
                        val description: String,
                        val icon: String,
                        val date: Int,
                        val cityName: String,
                        val country: String,
                        val cityId: Int):Parcelable