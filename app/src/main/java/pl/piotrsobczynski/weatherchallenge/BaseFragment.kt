package pl.piotrsobczynski.weatherchallenge

import android.support.v4.app.Fragment
import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import javax.inject.Inject

abstract class BaseFragment<P : BaseContract.Presenter> : Fragment(), BaseContract.View {

    @Inject
    protected lateinit var presenter: P

    /**
     * Inflating layout into fragment
     */
    @LayoutRes
    protected abstract fun getLayoutId(): Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(getLayoutId(), container, false)
    }

    //** IMPLEMENTATION **
    /**
     * hide soft keyboard
     */
    override fun hideKeyboard() {
        val inputMethodManager: InputMethodManager = this.context!!
                .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromInputMethod(view!!.windowToken, 0)
    }
}