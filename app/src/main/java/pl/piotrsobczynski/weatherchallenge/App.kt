package pl.piotrsobczynski.weatherchallenge

import android.app.Application
import pl.piotrsobczynski.weatherchallenge.di.component.AppComponent
import pl.piotrsobczynski.weatherchallenge.di.component.DaggerAppComponent
import pl.piotrsobczynski.weatherchallenge.di.module.AppModule

class App : Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
    }
//    lateinit var citiesFragmentSubcomponent: CitiesFragmentSubcomponent
//    lateinit var weatherFragmentSubcomponent: WeatherFragmentSubcomponent

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
//        setupTimber()
    }

//    private fun setupTimber() {
//        if (BuildConfig.DEBUG) {
//            Timber.plant(Timber.DebugTree())
//        } else {
//            Timber.plant(NoLoggingTimberTree())
//        }
//    }

//    fun citiesFragmentSubcomponent(): CitiesFragmentSubcomponent {
//        if (citiesFragmentSubcomponent == null) {
//            citiesFragmentSubcomponent = appComponent.plus(CitiesFragmentModule())
//        }
//        return citiesFragmentSubcomponent
//    }
//
//    fun weatherFragmentSubcomponent(): WeatherFragmentSubcomponent {
//        if (weatherFragmentSubcomponent == null) {
//            weatherFragmentSubcomponent = appComponent.plus(WeatherFragmentModule())
//        }
//        return weatherFragmentSubcomponent
//    }

}