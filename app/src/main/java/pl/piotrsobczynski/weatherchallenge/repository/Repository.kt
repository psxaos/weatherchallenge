package pl.piotrsobczynski.weatherchallenge.repository

import io.reactivex.Observable
import pl.piotrsobczynski.weatherchallenge.model.WeatherModel

interface Repository {
    /**
     * gets weather data on Find City button clicked
     */
    fun getWeatherList(city: String): Observable<List<WeatherModel>>

    /**
     * gets current weather for chosen city
     */
    fun getWeatherForCity(cityId: String): Observable<WeatherModel>
}