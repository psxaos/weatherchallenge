package pl.piotrsobczynski.weatherchallenge.repository.remote

import io.reactivex.Observable
import pl.piotrsobczynski.weatherchallenge.model.WeatherResponseModel

interface Remote {
    fun fetchWeatherFromRemote(city: String): Observable<WeatherResponseModel>
}