package pl.piotrsobczynski.weatherchallenge.repository.remote

import io.reactivex.Observable
import pl.piotrsobczynski.weatherchallenge.model.WeatherResponseModel
import javax.inject.Inject

class RemoteImpl @Inject constructor(private val weatherApi: WeatherApi) : Remote {

    override fun fetchWeatherFromRemote(city: String): Observable<WeatherResponseModel> {
        return weatherApi.fetchListForecast(city)
    }

}