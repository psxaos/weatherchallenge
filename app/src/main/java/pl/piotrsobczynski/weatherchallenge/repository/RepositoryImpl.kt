package pl.piotrsobczynski.weatherchallenge.repository

import io.reactivex.Observable
import pl.piotrsobczynski.weatherchallenge.model.WeatherModel
import pl.piotrsobczynski.weatherchallenge.repository.remote.Remote
import pl.piotrsobczynski.weatherchallenge.utils.LogUtil
import pl.piotrsobczynski.weatherchallenge.utils.SchedulerProvider
import javax.inject.Inject

class RepositoryImpl @Inject constructor(private var remote: Remote,
                                         private val schedulerProvider: SchedulerProvider,
                                         private val responseMapper: WeatherResponseMapper
) : Repository {
    private val TAG: String = "RepositoryImpl"
    /**
    logic for getting data from two sources, for now we get only from remote
     */
    override fun getWeatherList(city: String): Observable<List<WeatherModel>> {
        LogUtil.d(TAG, "getWeatherList() called with: city = [$city]")

        return remote.fetchWeatherFromRemote(city)
                .map {
                    it.forecasts.map {
                        responseMapper.mapFromResponse(it)
                    }
                }
    }

    override fun getWeatherForCity(cityId: String): Observable<WeatherModel> {

        return Observable.empty()
    }
}