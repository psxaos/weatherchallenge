package pl.piotrsobczynski.weatherchallenge.repository.cache

import io.reactivex.Observable
import pl.piotrsobczynski.weatherchallenge.model.WeatherModel

interface Cache {
    /**
     * gets weather data on Find City button clicked
     */
    fun getWeatherList(): Observable<List<WeatherModel>>

    /**
     * gets current weather for chosen city
     */
    fun getWeatherForCity(cityId: String): Observable<WeatherModel>
}