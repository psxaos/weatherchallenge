package pl.piotrsobczynski.weatherchallenge.repository.remote

import io.reactivex.Observable
import pl.piotrsobczynski.weatherchallenge.model.WeatherResponseModel
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("find")
    fun fetchListForecast(@Query("q") city: String):
            Observable<WeatherResponseModel>

    @GET("id")
    fun fetchForecastForCity(@Query("q") cityId: String):
            Observable<WeatherResponseModel>

}