package pl.piotrsobczynski.weatherchallenge.repository

interface ModelMapper<in R, out C> {
    fun mapFromResponse(response: R): C
}