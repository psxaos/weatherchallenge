package pl.piotrsobczynski.weatherchallenge.repository

import pl.piotrsobczynski.weatherchallenge.model.Forecast
import pl.piotrsobczynski.weatherchallenge.model.WeatherModel
import javax.inject.Inject

class WeatherResponseMapper @Inject constructor(): ModelMapper<Forecast, WeatherModel> {
    override fun mapFromResponse(response: Forecast): WeatherModel {

        return WeatherModel(response.main.temp,
                response.main.temp_min,
                response.main.temp_max,
                response.main.humidity,
                response.main.pressure,
                response.weather[0].description,
                response.weather[0].icon,
                response.date,
                response.cityName,
                response.sys.country,
                response.cityId)
    }
}