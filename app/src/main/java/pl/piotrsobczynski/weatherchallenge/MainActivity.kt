package pl.piotrsobczynski.weatherchallenge

import android.os.Bundle
import pl.piotrsobczynski.weatherchallenge.model.WeatherModel
import pl.piotrsobczynski.weatherchallenge.usecase.displaycities.CitiesContract
import pl.piotrsobczynski.weatherchallenge.usecase.displaycities.CitiesFragment
import pl.piotrsobczynski.weatherchallenge.usecase.displayweather.WeatherFragment
import pl.piotrsobczynski.weatherchallenge.utils.Constants
import pl.piotrsobczynski.weatherchallenge.utils.LogUtil

class MainActivity : BaseActivity(), CitiesContract.ParentListener {
    private val TAG: String = "MainActivity"

    //** LIFECYCLE **
    override fun onCreate(savedInstanceState: Bundle?) {
        LogUtil.d(TAG, "onCreate() called with: savedInstanceState = [$savedInstanceState]")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val citiesFragment: CitiesFragment = CitiesFragment()
        replaceFragment(citiesFragment, citiesFragment.TAG, R.id.mainFrameLayout)

    }


//** END LIFECYCLE **

    override fun setLayoutId(): Int {
        return R.layout.activity_main
    }

    //IMPLEMENTATION
    override fun onCityClicked(cityItem: WeatherModel) {
        val weatherFragment = WeatherFragment()
        val bundle = Bundle()
        bundle.putParcelable(Constants.WEATHER_KEY, cityItem)
        weatherFragment.arguments = bundle
        replaceFragment(weatherFragment, weatherFragment.TAG, R.id.mainFrameLayout)
    }

}