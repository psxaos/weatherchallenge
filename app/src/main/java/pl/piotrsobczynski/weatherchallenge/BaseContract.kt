package pl.piotrsobczynski.weatherchallenge

abstract class BaseContract {

    interface Presenter{
        fun dispose()
    }

    interface View {
        fun hideKeyboard()
    }
}