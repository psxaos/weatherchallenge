package pl.piotrsobczynski.weatherchallenge.usecase.displayweather

import pl.piotrsobczynski.weatherchallenge.BaseContract
import pl.piotrsobczynski.weatherchallenge.model.WeatherModel

class WeatherContract : BaseContract() {

    interface Presenter : BaseContract.Presenter {
        fun attachView(view: WeatherContract.View)
        fun onWeatherReceived(weather: WeatherModel?)
    }

    interface View : BaseContract.View {
        fun displayWeather(weatherModel: WeatherModel?)
        fun showError(e: String)
    }
}