package pl.piotrsobczynski.weatherchallenge.usecase.displaycities

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.rv_cities_item.view.*
import pl.piotrsobczynski.weatherchallenge.R
import pl.piotrsobczynski.weatherchallenge.model.WeatherModel
import pl.piotrsobczynski.weatherchallenge.utils.LogUtil
import javax.inject.Inject

class CitiesRecyclerAdapter @Inject constructor(var weatherList: MutableList<WeatherModel>,
                                                val cityItemListener: CityItemListener)
    : RecyclerView.Adapter<CitiesRecyclerAdapter.ViewHolder>() {
    private val TAG: String = "CitiesRecyclerAdapter"

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int)
            : ViewHolder {
        LogUtil.d(TAG, "onCreateViewHolder() called with: parent = [$parent], viewType = [$viewType]")

        val itemView: View = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.rv_cities_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        var size = weatherList.size
        LogUtil.d(TAG, "getItemCount() called, size: $size")
        return size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        LogUtil.d(TAG, "onBindViewHolder() called with: holder = [$holder], position = [$position]")
        val cityItem: WeatherModel = weatherList[position]

        holder.cityName.text = cityItem.cityName
        holder.country.text = cityItem.country
        holder.cityListItem.setOnClickListener({cityItemListener.onClick(cityItem)})

    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var cityListItem: ConstraintLayout
        var cityName: TextView
        var country: TextView

        init {
            LogUtil.d(TAG, "init ViewHolder called");
            cityListItem = view.citiesItemLayout
            cityName = view.rvItemCityName
            country = view.rvItemCountry
        }

    }

    interface CityItemListener {
        fun onClick(cityItem: WeatherModel)
    }
}