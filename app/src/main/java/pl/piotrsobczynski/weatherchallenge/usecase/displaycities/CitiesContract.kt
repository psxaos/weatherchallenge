package pl.piotrsobczynski.weatherchallenge.usecase.displaycities

import pl.piotrsobczynski.weatherchallenge.BaseContract
import pl.piotrsobczynski.weatherchallenge.model.WeatherModel

class CitiesContract {

    interface Presenter : BaseContract.Presenter {
        fun attachView(view: CitiesContract.View)
        fun getWeatherList(city: String)
        fun getWeatherListforRv(): MutableList<WeatherModel>
    }

    interface View : BaseContract.View {
        fun displayCitiesList(cities: List<WeatherModel>)
        fun showError(e: String)
    }

    /**
     * Interface for communicating to Parent Activity
     */
    interface ParentListener {
        fun onCityClicked(cityItem: WeatherModel)
    }
}