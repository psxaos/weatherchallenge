package pl.piotrsobczynski.weatherchallenge.usecase.displayweather

import io.reactivex.disposables.CompositeDisposable
import pl.piotrsobczynski.weatherchallenge.BasePresenter
import pl.piotrsobczynski.weatherchallenge.model.WeatherModel
import pl.piotrsobczynski.weatherchallenge.utils.SchedulerProvider
import javax.inject.Inject

class WeatherPresenter @Inject constructor(val scheduler: SchedulerProvider
) : BasePresenter(), WeatherContract.Presenter {
    private val TAG: String = "WeatherPresenter"

    lateinit var view: WeatherContract.View
    private val disposables: CompositeDisposable = CompositeDisposable()

    //IMPLEMENTATION
    override fun onWeatherReceived(weather: WeatherModel?) {
        if (weather != null) {
            view.displayWeather(weather)
        }

    }

    override fun attachView(view: WeatherContract.View) {
        this.view = view
    }

    override fun dispose() {
        disposables.dispose()
    }
}