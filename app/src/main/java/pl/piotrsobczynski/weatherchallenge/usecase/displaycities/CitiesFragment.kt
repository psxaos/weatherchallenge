package pl.piotrsobczynski.weatherchallenge.usecase.displaycities

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.cities_fragment.*
import pl.piotrsobczynski.weatherchallenge.App
import pl.piotrsobczynski.weatherchallenge.BaseFragment
import pl.piotrsobczynski.weatherchallenge.R
import pl.piotrsobczynski.weatherchallenge.di.component.CitiesFragmentSubcomponent
import pl.piotrsobczynski.weatherchallenge.di.module.CitiesFragmentModule
import pl.piotrsobczynski.weatherchallenge.model.WeatherModel
import pl.piotrsobczynski.weatherchallenge.utils.LogUtil

class CitiesFragment : BaseFragment<CitiesContract.Presenter>(),
        CitiesContract.View, CitiesRecyclerAdapter.CityItemListener {
    val TAG: String = "CitiesFragment"

    val component: CitiesFragmentSubcomponent by lazy {
        (activity?.application as App)
                .appComponent
                .plus(CitiesFragmentModule())
    }
    lateinit var parent: CitiesContract.ParentListener
    lateinit var mCitiesRecyclerAdapter: CitiesRecyclerAdapter

    //LIFECYCLE
    override fun onAttach(context: Context?) {
        LogUtil.d(TAG, "onAttach() called with: context = [$context]")
        super.onAttach(context)
        try {
            this.parent = context as CitiesContract.ParentListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + "Must implement CitiesContract.ParentListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        LogUtil.d(TAG, "onCreate() called with: savedInstanceState = [$savedInstanceState]")
        super.onCreate(savedInstanceState)
        //init Dagger
        component.inject(this)
        presenter.attachView(this)

    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?)
            : View {
        LogUtil.d(TAG, "onCreateView() called with: inflater = [$inflater], container = [$container], savedInstanceState = [$savedInstanceState]")

        val view = super.onCreateView(inflater, container, savedInstanceState)
        mCitiesRecyclerAdapter = CitiesRecyclerAdapter(presenter.getWeatherListforRv(), this)
        //TODO: fill recycler with cached data
        val citiesListRv = view.let { it.findViewById(R.id.rvCities) as RecyclerView }
        citiesListRv.layoutManager = LinearLayoutManager(context)
        citiesListRv.adapter = mCitiesRecyclerAdapter

        return view
    }

    override fun onStart() {
        super.onStart()
        initListeners()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dispose()
    }

    //END LIFECYCLE

    //IMPLEMENTATION
    override fun onClick(cityItem: WeatherModel) {
        //TODO go to WeatherFragment
        parent.onCityClicked(cityItem)
    }

    override fun getLayoutId(): Int {
        return R.layout.cities_fragment
    }

    override fun showError(e: String) {
        Toast.makeText(context, e, Toast.LENGTH_LONG).show()
    }

    override fun displayCitiesList(cities: List<WeatherModel>) {
        LogUtil.d(TAG, "displayCitiesList() called with: cities = [$cities]")
        mCitiesRecyclerAdapter.notifyDataSetChanged()

    }

    fun initListeners() {

        buttonCity.setOnClickListener({ presenter.getWeatherList(etCity.text.toString()) })
    }
}