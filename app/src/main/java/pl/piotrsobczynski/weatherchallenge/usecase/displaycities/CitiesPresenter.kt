package pl.piotrsobczynski.weatherchallenge.usecase.displaycities

import io.reactivex.disposables.CompositeDisposable
import pl.piotrsobczynski.weatherchallenge.BasePresenter
import pl.piotrsobczynski.weatherchallenge.model.WeatherModel
import pl.piotrsobczynski.weatherchallenge.repository.Repository
import pl.piotrsobczynski.weatherchallenge.utils.LogUtil
import pl.piotrsobczynski.weatherchallenge.utils.SchedulerProvider
import javax.inject.Inject

class CitiesPresenter @Inject constructor(val repository: Repository,
                                          val scheduler: SchedulerProvider)
    : BasePresenter(), CitiesContract.Presenter {
    private val TAG: String = "CitiesPresenter"

    var mWeatherList: MutableList<WeatherModel> = mutableListOf()
    lateinit var view: CitiesContract.View
    private val disposables: CompositeDisposable = CompositeDisposable()


    //IMPLEMENTATION
    override fun attachView(view: CitiesContract.View) {
        LogUtil.d(TAG, "attachView() called with: view = [$view]")
        this.view = view
    }

    override fun getWeatherList(city: String) {
        LogUtil.d(TAG, "getWeatherList() called with: city = [$city]")
        val observable = repository.getWeatherList(city)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.mainThread())
                .subscribe(this::onWeatherListReceived, this::onErrorReceived)
        disposables.add(observable)
    }

    override fun getWeatherListforRv(): MutableList<WeatherModel> {
        return mWeatherList
    }

    override fun dispose() {
        LogUtil.d(TAG, "dispose() called");
        disposables.dispose()
    }

    fun onWeatherListReceived(weatherList: List<WeatherModel>) {
        LogUtil.d(TAG, "onWeatherListReceived() called with: weatherList = [$weatherList]")
        LogUtil.d(TAG, weatherList.toString())
        mWeatherList.clear()
        mWeatherList.addAll(weatherList)
        view.displayCitiesList(weatherList)
    }

    fun onErrorReceived(t: Throwable) {
        LogUtil.d(TAG, "onErrorReceived() called with: t = [$t]")
        t.message?.let { view.showError(it) }
    }
}