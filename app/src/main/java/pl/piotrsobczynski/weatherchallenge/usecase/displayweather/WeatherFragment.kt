package pl.piotrsobczynski.weatherchallenge.usecase.displayweather

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.weather_details_section.*
import kotlinx.android.synthetic.main.weather_fragment.*
import pl.piotrsobczynski.weatherchallenge.App
import pl.piotrsobczynski.weatherchallenge.BaseFragment
import pl.piotrsobczynski.weatherchallenge.R
import pl.piotrsobczynski.weatherchallenge.di.component.WeatherFragmentSubcomponent
import pl.piotrsobczynski.weatherchallenge.di.module.WeatherFragmentModule
import pl.piotrsobczynski.weatherchallenge.model.WeatherModel
import pl.piotrsobczynski.weatherchallenge.utils.Constants
import pl.piotrsobczynski.weatherchallenge.utils.LogUtil
import pl.piotrsobczynski.weatherchallenge.utils.Utils
import kotlin.math.roundToInt

class WeatherFragment : BaseFragment<WeatherContract.Presenter>(), WeatherContract.View {
    val TAG: String = "WeatherFragment"

    val component: WeatherFragmentSubcomponent by lazy {
        (activity?.application as App)
                .appComponent
                .plus(WeatherFragmentModule())
    }

    //LIFECYCLE
    override fun onCreate(savedInstanceState: Bundle?) {
        LogUtil.d(TAG, "onCreate() called with: savedInstanceState = [$savedInstanceState]")
        super.onCreate(savedInstanceState)
        //init Dagger
        component.inject(this)

        presenter.attachView(this)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?)
            : View {
        LogUtil.d(TAG, "onCreateView() called with: inflater = [$inflater], container = [$container], savedInstanceState = [$savedInstanceState]")

        val view = super.onCreateView(inflater, container, savedInstanceState)


        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val bundle: Bundle? = this.arguments

        bundle?.let {
            if (it.containsKey(Constants.WEATHER_KEY)) {
                val weatherDataToDisplay: WeatherModel? = this.arguments?.let {
                    it.getParcelable(Constants.WEATHER_KEY)
                }
                presenter.onWeatherReceived(weatherDataToDisplay)
            }
        }
    }

    override fun onDestroy() {
        LogUtil.d(TAG, "onDestroy() called")
        super.onDestroy()
        presenter.dispose()
    }

    //END LIFECYCLE

    //IMPLEMENTATION
    override fun showError(e: String) {
        LogUtil.d(TAG, "showError() called with: e = [$e]")
        Toast.makeText(context, e, Toast.LENGTH_LONG).show()
    }

    override fun getLayoutId(): Int {
        LogUtil.d(TAG, "getLayoutId() called")
        return R.layout.weather_fragment

    }

    override fun displayWeather(weatherModel: WeatherModel?) {
        LogUtil.d(TAG, "displayWeather() called with: weatherModel = [$weatherModel]");
        //TODO:if no data display info instead od dummy data
        tvWeatherCityName.text = weatherModel?.cityName ?: getString(R.string.no_data_to_display)
        tvWeatherTemperature.text = weatherModel?.temperature?.roundToInt()?.toString() ?: getString(R.string.no_text)
        tvWeatherDescription.text = weatherModel?.description ?: getString(R.string.no_text)

        Glide.with(this)
                .load((Constants.ICON_URL + "${weatherModel?.icon}.png"))
                .apply(RequestOptions().override(128, 128)
                        .placeholder(R.drawable.no_weather_placeholder_ic))
                .into(weatherIcon)

        tvTemperatureMin.text = weatherModel?.tempMin?.toString() ?: getString(R.string.no_text)
        tvTemperatureMax.text = weatherModel?.tempMax?.toString() ?: getString(R.string.no_text)
        tvHumidity.text = weatherModel?.humidity?.toString() ?: getString(R.string.no_text)
        tvPressure.text = weatherModel?.pressure?.roundToInt()?.toString() ?: getString(R.string.no_text)
        tvDate.text = Utils.getDate(weatherModel?.date?.toLong())

    }
}