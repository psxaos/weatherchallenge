package pl.piotrsobczynski.weatherchallenge

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import pl.piotrsobczynski.weatherchallenge.utils.LogUtil

abstract class BaseActivity : AppCompatActivity() {
    private val TAG: String = "BaseActivity"

    @LayoutRes
    protected abstract fun setLayoutId(): Int


    //LIFECYCLE
    override fun onCreate(savedInstanceState: Bundle?) {
        LogUtil.d(TAG, "onCreate() called with: savedInstanceState = [$savedInstanceState]")
        super.onCreate(savedInstanceState)
        setContentView(setLayoutId())
    }

    override fun onDestroy() {
        LogUtil.d(TAG, "onDestroy() called")
        super.onDestroy()
    }

    //END LIFECYCLE

    fun replaceFragment(fragment: Fragment, TAG: String, @IdRes containerId: Int) {
        LogUtil.d(TAG, "replaceFragment() called with: fragment = [$fragment], TAG = [$TAG], containerId = [$containerId]")
        val fragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(containerId, fragment, TAG)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    override fun onBackPressed() {
        LogUtil.d(TAG, "onBackPressed() called")
        if (supportFragmentManager.backStackEntryCount <= 1) {
            finish()
        } else {
            super.onBackPressed()
        }
    }


    /**
     * Checks the network connection state
     *
     * @return
     */
    internal fun isNetworkConnected(): Boolean {
        LogUtil.d(TAG, "isNetworkConnected() called")
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

}