package pl.piotrsobczynski.weatherchallenge.utils

import java.text.SimpleDateFormat
import java.util.*

object Utils {
    fun getDate(date: Long?): String {
        val timeFormatter = SimpleDateFormat("dd.MM.yyyy")
        if (date != null) {
            return timeFormatter.format(Date(date * 1000L))
        } else {
            return ""
        }
    }
}
