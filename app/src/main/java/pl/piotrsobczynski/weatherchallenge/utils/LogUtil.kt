package pl.piotrsobczynski.weatherchallenge.utils

import android.util.Log

class LogUtil {
    companion object {
        fun d(tag: String, msg: String) {
            Log.d(tag, msg)
        }

        fun i(tag: String, msg: String) {
            Log.i(tag, msg)
        }

        fun v(tag: String, msg: String) {
            Log.v(tag, msg)
        };
        fun w(tag: String, msg: String) {
            Log.w(tag, msg)
        };
        fun e(tag: String, msg: String) {
            Log.e(tag, msg)
        };
    }
}


//class NoLoggingTimberTree : Timber.Tree() {
//
//    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
//    }
//}