package pl.piotrsobczynski.weatherchallenge.utils

import pl.piotrsobczynski.weatherchallenge.BuildConfig

object Constants {

    const val BASE_URL = BuildConfig.BASE_URL
    const val API_KEY = BuildConfig.API_KEY
    const val ICON_URL = "http://openweathermap.org/img/w/" //append with .png
    const val WEATHER_KEY = "WEATHER_DATA" //Key value used in bundle when passing this object between fragments
    const val NO_WEATHER_ICON_URL = "https://cdn2.iconfinder.com/data/icons/line-weather/130/No_Data-128.png"
}