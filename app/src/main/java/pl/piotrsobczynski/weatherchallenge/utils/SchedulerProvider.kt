package pl.piotrsobczynski.weatherchallenge.utils

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
//TODO to be removed, instead we introduced dagger injection of rx schedulers
class SchedulerProvider {

    fun io() = Schedulers.io()

    fun mainThread() = AndroidSchedulers.mainThread()

    fun computation() = Schedulers.computation()

}